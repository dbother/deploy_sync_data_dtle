#!/bin/bash
function install_docker() {
        yum install -y yum-utils device-mapper-persistent-data lvm2 net-tools vim
        yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
        yum -y install docker-ce
        systemctl start docker && systemctl enable docker
        docker_resource

}

function docker_resource() {
        cat >> /etc/docker/daemon.json <<EOF
{
  "registry-mirrors": ["https://4z8t423f.mirror.aliyuncs.com"],
  "exec-opts": ["native.cgroupdriver=systemd"]
}
EOF
        systemctl daemon-reload
        systemctl restart docker
}

install_docker