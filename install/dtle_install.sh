#!/bin/bash

# 作者：杨大大
# 说明：部署 docker 版本的 DTLE

CONSUL_HTTP_PORT=8500
NOMAD_HTTP_PORT=4646
DTLE_2XHTTP_API_PORT=8190


CURRENT_DIR=$(cd `dirname $0`; pwd)
SCRIPTS_DIR=$(cd `dirname $0`;cd ..; pwd)/scripts

function install_dtle_network(){
    docker network create dtle-net
}

function install_dtle_consul(){
    #docker run --name dtle-consul --network=dtle-net -p ${CONSUL_HTTP_PORT}:8500 -d consul:latest
    docker run --name dtle-consul --network=dtle-net -p ${CONSUL_HTTP_PORT}:8500 -d registry.cn-hangzhou.aliyuncs.com/dbrother/syncdata:dtle_consul_1.11.1_v1
}

function install_dtle(){
    #docker run --name dtle --network=dtle-net -p ${NOMAD_HTTP_PORT}:4646 -p ${DTLE_2XHTTP_API_PORT}:8190 -d actiontech/dtle
    docker run --name dtle --network=dtle-net -p ${NOMAD_HTTP_PORT}:4646 -p ${DTLE_2XHTTP_API_PORT}:8190 -d registry.cn-hangzhou.aliyuncs.com/dbrother/syncdata:dtle_4.23.04.2_v1
}

function install_jq(){
   yum install -y jq
}

function create_script_dir(){
  mkdir -p $SCRIPTS_DIR
}

function crete_script_add_job(){
    cat > $SCRIPTS_DIR/add_job.sh << EOF
#!/bin/bash

param=\$1
if [ \$param ];then
  curl -XPOST 127.0.0.1:${NOMAD_HTTP_PORT}/v1/jobs -d @\$param | jq
else
  echo "\$0 [xxx.json]"
fi
  
EOF

  chmod +x $SCRIPTS_DIR/add_job.sh
}

function crete_script_del_job(){
    cat > $SCRIPTS_DIR/del_job.sh << EOF
#!/bin/bash

param=\$1
if [ \$param ];then
  curl -XDELETE 127.0.0.1:${NOMAD_HTTP_PORT}/v1/job/\$param?purge=true | jq
  curl -XDELETE "127.0.0.1:${CONSUL_HTTP_PORT}/v1/kv/dtle/\$param?recurse" && echo ""
else
  echo "\$0 [xxx.json]"
fi
  
EOF

  chmod +x $SCRIPTS_DIR/del_job.sh
}

function crete_script_pause_job(){
    cat > $SCRIPTS_DIR/pause_job.sh << EOF
#!/bin/bash

param=\$1
if [ \$param ];then
  curl -XDELETE 127.0.0.1:${NOMAD_HTTP_PORT}/v1/job/\$param?purge=true | jq
else
  echo "\$0 [xxx.json]"
fi
  
EOF

  chmod +x $SCRIPTS_DIR/pause_job.sh
}

function crete_script_resume_job(){
    cat > $SCRIPTS_DIR/resume_job.sh << EOF
#!/bin/bash

param=\$1
if [ \$param ];then
  curl -XPOST 127.0.0.1:${NOMAD_HTTP_PORT}/v1/jobs -d @\$param | jq
else
  echo "\$0 [xxx.json]"
fi
  
EOF

  chmod +x $SCRIPTS_DIR/resume_job.sh
}

function create_scrpit_list_jobs_name(){
    cat > $SCRIPTS_DIR/list_jobs_name.sh << EOF
#!/bin/bash
curl -XGET 127.0.0.1:${NOMAD_HTTP_PORT}/v1/jobs | jq '.' | grep -w '.Name'
EOF

  chmod +x $SCRIPTS_DIR/list_jobs_name.sh
}

function create_script_status_job_running_status(){
    cat > $SCRIPTS_DIR/status_job_running_status.sh << EOF
#!/bin/bash

param=\$1
if [ \$param ];then
  curl -XGET "http://127.0.0.1:${NOMAD_HTTP_PORT}/v1/job/\$param" -s | jq '.Status'
else
  echo "\$0 [job_name]"
fi

EOF

  chmod +x $SCRIPTS_DIR/status_job_running_status.sh   
}

function create_script_status_job_full_or_incr(){
    cat > $SCRIPTS_DIR/status_job_full_or_incr.sh << EOF
#!/bin/bash

param=\$1
if [ \$param ];then
  curl -XGET "127.0.0.1:${CONSUL_HTTP_PORT}/v1/kv/dtle/\$param/JobStage?raw" && echo ""
else
  echo "\$0 [job_name]"
fi

EOF

  chmod +x $SCRIPTS_DIR/status_job_full_or_incr.sh
}

function create_script_status_job_gtid_already_exec(){
    cat > $SCRIPTS_DIR/status_job_gtid_already_exec.sh << EOF
#!/bin/bash

param=\$1
if [ \$param ];then
  curl -XGET "127.0.0.1:${CONSUL_HTTP_PORT}/v1/kv/dtle/\$param/Gtid?raw" && echo ""
else
  echo "\$0 [job_name]"
fi

EOF

  chmod +x $SCRIPTS_DIR/status_job_gtid_already_exec.sh    
}

function create_script_status_job_dump_progress(){
    cat > $SCRIPTS_DIR/status_job_dump_progress.sh << EOF
#!/bin/bash

param=\$1
if [ \$param ];then
  curl -XGET "127.0.0.1:${CONSUL_HTTP_PORT}/v1/kv/dtle/\$param/DumpProgress?raw" && echo ""
else
  echo "\$0 [job_name]"
fi

EOF

  chmod +x $SCRIPTS_DIR/status_job_dump_progress.sh   
}

function install_dtle_all(){
    echo "创建dtle_network"
    install_dtle_network
    echo "安装dtle_consul"
    install_dtle_consul
    echo "安装dtle组件"
    install_dtle
    echo "安装 jq"
    install_jq
    echo "安装完成"
}

function create_scripts(){
    echo "生成操作脚本 $SCRIPTS_DIR/"
    create_script_dir
    crete_script_add_job
    crete_script_del_job
    crete_script_pause_job
    crete_script_resume_job
    create_scrpit_list_jobs_name
    create_script_status_job_full_or_incr
    create_script_status_job_gtid_already_exec
    create_script_status_job_running_status
    create_script_status_job_dump_progress
    echo "脚本生成完成"
}

function install_monitor(){
    # 部署 prometheus
    echo ""
    # 部署 grafana
}

function clean_dtle(){
    docker stop dtle dtle-consul
    docker rm dtle dtle-consul
    docker network rm dtle-net
}

# main 入口
command="${1}"
case "${command}" in
    "dtle" )
        install_dtle_all
        create_scripts
    ;;
    "clean" )
        echo "删除dtle"
        clean_dtle
        echo "删除完成"
    ;;
    * )
        echo "
            $0 dtle  部署 dtle
            $0 clean 删除 dtle 环境
        "
    ;;
esac

