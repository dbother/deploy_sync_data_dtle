## 说明
> * 有时候会有需求实现 MySQL 某个库或表 实时同步到另一个库的需求，使用 dtle 可以实现这个操作
> * 本例使用脚本一键安装，且给出了配置demo，能方便快速的上手操作dtle

使用 docker 部署 dtle


* demo_json  : 内含几个dtle同步demo
* install : 安装脚本
* scripts : 便捷操作脚本，执行安装后生成

## 注意：

- 对于源端和目标端为同一MySQL实例时，一定要使用 SkipCreateDbTable 参数，自行手动创建同步目标的库表信息，否则有hang住整个库的风险(bug:https://github.com/actiontech/dtle/issues/1060)

## MySQL权限说明

源端：

```sql
create user  'dts_src'@'%' identified by '123456';
grant select,replication client,replication slave on *.* to 'dts_src'@'%';
```

目标端：

```sql
# 如果不想给库级别的create语句，需要目标端先创建一个 dtle 名称的库(dtle 需要一个库)
create database dtle default character set utf8mb4;

# 创建用户并授权
create user 'dtle_dst'@'%' identified by '123456';
# 例如同步到 abc 库，则将abc库相关权限授予
grant alter,create,index,references,insert,delete,update,select,trigger on abc.* to 'dtle_dst'@'%';
```

## 安装
>  未安装docker的请先执行 docker_install.sh 安装docker环境

```shell
[root@init install]# ./dtle_install.sh dtle
创建dtle_network
45832da0b62efb2d3090a7df652116e4fb7c8ac226a094fd48b2ae734f29ebe4
安装dtle_consul
9026dfde5f59445d592f48838604c6499c0c8878b7896e935c603212099874a8
安装dtle组件
dd7724fae1c1ed9fc6cbb1cb6a394c0961cb25a5548de941b36939c76173a805
安装完成
生成操作脚本 /root/Deploy_SyncData_Dtle/scripts/
脚本生成完成
```

## web端查看
* http://部署服务器IP:4646
* http://部署服务器IP:8500

## 常用操作

首先修改好自定义的 json文件

### 添加job
```shell
[root@init scripts]# ./add_job.sh ../demo_json/table_to_table.json 
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  2832  100   151  100  2681   7416   128k --:--:-- --:--:-- --:--:--  130k
{
  "EvalID": "d1e22727-c4d4-8770-d94e-784cbfb59671",
  "EvalCreateIndex": 27,
  "JobModifyIndex": 27,
  "Warnings": "",
  "Index": 27,
  "LastContact": 0,
  "KnownLeader": false
}
```

### 查看job 名称列表信息
```shell
[root@init scripts]# ./list_jobs_name.sh 
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   650  100   650    0     0   268k      0 --:--:-- --:--:-- --:--:--  317k
    "Name": "db_sync_tables",
```

### 查看job运行状态
```shell
[root@init scripts]# ./status_job_running_status.sh db_sync_tables 
"running"
```

### 查看当前job处于全量还是增量状态
```shell
[root@init scripts]# ./status_job_full_or_incr.sh db_sync_tables
job_stage_incr
```

### 查看job执行到的gtid值为多少
```shell
[root@init scripts]# ./status_job_gitd_already_exec.sh db_sync_tables
299338b0-f933-11ed-9b94-0242ac110002:1-6313
```

### 暂停任务
其实暂停任务就是将任务删除掉，但是不清除consul信息
```shell
[root@init scripts]# ./pause_job.sh db_sync_tables
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   175  100   175    0     0  15255      0 --:--:-- --:--:-- --:--:-- 15909
{
  "EvalID": "6db6914a-6698-21da-6d6a-a712fe5fd8ef",
  "EvalCreateIndex": 76,
  "JobModifyIndex": 76,
  "VolumeEvalID": "",
  "VolumeEvalIndex": 0,
  "Index": 76,
  "LastContact": 0,
  "KnownLeader": false
}
```

### 恢复任务
> 注意： 恢复任务需要原来的json还在
```shell
[root@init scripts]# ./resume_job.sh ../demo_json/table_to_table.json 
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  2832  100   151  100  2681  26730   463k --:--:-- --:--:-- --:--:--  523k
{
  "EvalID": "204301e7-d10d-be5e-b8d4-37c0b52d3fed",
  "EvalCreateIndex": 82,
  "JobModifyIndex": 82,
  "Warnings": "",
  "Index": 82,
  "LastContact": 0,
  "KnownLeader": false
}
```

### 彻底删除任务
再次重新添加相同的任务会从设置的初始值开始进行全量+增量
```shell
[root@init scripts]# ./del_job.sh db_sync_tables
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   175  100   175    0     0   8364      0 --:--:-- --:--:-- --:--:--  8750
{
  "EvalID": "9cbefa2b-e9d1-fd9b-d84b-d6454bffa068",
  "EvalCreateIndex": 94,
  "JobModifyIndex": 94,
  "VolumeEvalID": "",
  "VolumeEvalIndex": 0,
  "Index": 94,
  "LastContact": 0,
  "KnownLeader": false
}
true
```